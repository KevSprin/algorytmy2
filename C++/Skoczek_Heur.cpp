#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>

using namespace std;

class FieldInfo{
    private:
        int cost;
        int x;
        int y;
    public:
        FieldInfo(){}
        FieldInfo(int cost, int x, int y){
            this->cost = cost;
            this->x = x;
            this->y = y;
        }
        int GetCost(){
            return cost;
        }
        int GetX(){
            return x;
        }
        int GetY(){
            return y;
        }
        bool operator<(FieldInfo other) const{
            return this->cost < other.cost;
        }
        bool operator>(FieldInfo other) const{
            return this->cost > other.cost;
        }
        bool operator<=(FieldInfo other) const{
            return this->cost <= other.cost;
        }
        bool operator>=(FieldInfo other) const{
            return this->cost >= other.cost;
        }
        bool operator==(FieldInfo other) const{
            return this->cost == other.cost;
        }
        ~FieldInfo(){}
};

class Skoczek{
    private:
        vector<int> ruch_x = {2, 1, -1, -2, -2, -1, 1, 2};
        vector<int> ruch_y = {1, 2, 2, 1, -1, -2, -2, -1};
        vector<int> trzy_x = {1, 0, -1, 0};
        vector<int> trzy_y = {0, 1, 0, -1};
        vector<int> cztery_x = {1, -1, -1, 1};
        vector<int> cztery_y = {1, 1, -1, -1};
        vector<int> dwa_x;
        vector<int> dwa_y;
        int pocz_x = 0, pocz_y = 0, wielkosc = 0, ruchy = 8;
        vector<vector<int>> szachownica;
        vector<vector<int>> dostepne_skoki;

        // Funkcja do wstawiania wartości w konkretne miejsce w tablicy
        bool WstawLiczbe(int x, int y, vector<vector<int>> &Tablica, int wartosc){
            if(x >= 0 && y >= 0 && x < wielkosc && y < wielkosc){
                Tablica[x][y] = wartosc;
                return true;
            }
            return false;
        }
        
        // Funkcja odpowiadająca za odświeżanie tablicy informacji o liczbie sąsiadów
        void DodajUsunSasiada(int x, int y, int flag){
            for(int i=0; i < ruch_x.size(); ++i){
                if(ruch_x[i] + x >= 0 && ruch_x[i] + x < wielkosc && ruch_y[i] + y >= 0 && ruch_y[i] + y < wielkosc){
                    dostepne_skoki[x+ruch_x[i]][y+ruch_y[i]] += flag;
                }
            }
        }

        // Generowanie tablicy informacji o liczbię sąsiadów
        void WypelnijDostepneSkoki(){

            // for O(4*4) 
            for(int i = 0; i < dwa_x.size(); ++i){
                WstawLiczbe(dwa_x[i],dwa_y[i],dostepne_skoki, 2);
                for(int j = 0; j < dwa_x.size(); ++j){
                    WstawLiczbe(dwa_x[i] + trzy_x[j], dwa_y[i] + trzy_y[j], dostepne_skoki, 3);
                    WstawLiczbe(dwa_x[i] + cztery_x[j], dwa_y[i] + cztery_y[j], dostepne_skoki, 4);
                }
            }
            // for O(n^2)
            for(int i = 0; i < wielkosc; ++i){
                for(int j = 0; j < wielkosc; ++j){
                    
                    // Dla 4
                    if( ((j > 1 && j < wielkosc-2) && i == 0) || ((j > 1 && j < wielkosc-2) && i == wielkosc-1) || ((i > 1 && i < wielkosc-2) && j == wielkosc-1) || ((i > 1 && i < wielkosc-2) && j == 0)){
                        dostepne_skoki[i][j] = 4;
                    }
                    // Dla 6
                    else if( ((j > 1 && j < wielkosc-2) && i == 1) || ((j > 1 && j < wielkosc-2) && i == wielkosc-2) || ((i > 1 && i < wielkosc-2) && j == wielkosc-2) || ((i > 1 && i < wielkosc-2) && j == 1)){
                        dostepne_skoki[i][j] = 6;
                    }
                    // Dla 8
                    else if ((i > 1 && j > 1) && (i < wielkosc-2 && j < wielkosc-2)){
                        dostepne_skoki[i][j] = 8;
                    }
                }
            }
        }
    public:
        Skoczek(){}
        Skoczek(int wielkosc){
            this->wielkosc = wielkosc;
            dwa_x = vector<int>{0, 0, wielkosc-1, wielkosc-1};
            dwa_y = vector<int>{0, wielkosc-1, 0, wielkosc-1};
            szachownica = vector<vector<int>>(wielkosc, vector<int>(wielkosc,0));
            dostepne_skoki = vector<vector<int>>(wielkosc, vector<int>(wielkosc));
            // Generowanie 
            WypelnijDostepneSkoki();
            
            // Wypełnienie i odświeżenie tablicy
            szachownica[pocz_x][pocz_y] = 1;
            DodajUsunSasiada(pocz_x, pocz_y, -1);
        }
        void UstawStart(int x, int y){
            szachownica[pocz_x][pocz_y] = 0;
            DodajUsunSasiada(pocz_x, pocz_y, 1);

            pocz_x = x;
            pocz_y = y;

            szachownica[pocz_x][pocz_y] = 1;
            DodajUsunSasiada(pocz_x, pocz_y, -1);
        }
        int GetX(){
            return pocz_x;
        }
        int GetY(){
            return pocz_y;
        }

        // Funkcja zwracająca tylko dopuszczalnych sąsiadów
        priority_queue<FieldInfo, vector<FieldInfo>, greater<FieldInfo>> ZwrocSasiadow(int x, int y){
            priority_queue<FieldInfo, vector<FieldInfo>, greater<FieldInfo>> sasiedzi;
            for(int i = 0; i < ruch_x.size(); ++i){
                int nowy_x = ruch_x[i] + x, nowy_y = ruch_y[i] + y;
                // jeśli nie wychodzimy poza zakres
                if(nowy_x >= 0 && nowy_x < wielkosc && nowy_y >= 0 && nowy_y < wielkosc){
                    // jeśli miejsce sąsiada jest wolne
                    if(szachownica[nowy_x][nowy_y] == 0){
                        int liczba_sasiadow = dostepne_skoki[nowy_x][nowy_y];
                        FieldInfo nowy_sasiad(liczba_sasiadow, nowy_x, nowy_y);
                        sasiedzi.push(nowy_sasiad);
                    }
                }
            }
            vector<FieldInfo> tmp;
            FieldInfo sasiad;
            do{
                sasiad = sasiedzi.top();
                sasiedzi.pop();
                tmp.push_back(sasiad);
            }while(!sasiedzi.empty() && sasiedzi.top() == sasiad);

            random_shuffle(tmp.begin(), tmp.end());
            for(auto &t : tmp){
                sasiedzi.push(t);
            }
            return sasiedzi;
        }

        // Funkcja wykonująca się rekurencyjnie wypełniająca szachownice skoczkiem
        void Skacz(int i, int x, int y, bool& q){
            int u = 0, v = 0;
            // zbierz wszystkich sąsiadów
            auto sasiedzi = ZwrocSasiadow(x, y); 
            do{                  
                // Warunek jeśli pójdzie coś nie tak
                if(sasiedzi.empty()){
                    Pokaz();
                    cout << "Cos poszlo nie tak!" << endl;
                    q = true;
                    return;
                }

                // wybierz najpierw najmniejszy
                // przekaż właśnie zajęte dalej do rekurencji
                FieldInfo sasiad = sasiedzi.top();
                sasiedzi.pop();

                int nowy_x = sasiad.GetX();
                int nowy_y = sasiad.GetY();             
                
                // przypisanie wartości do pola sąsiada
                szachownica[nowy_x][nowy_y] = i;
                DodajUsunSasiada(nowy_x, nowy_y, -1);
                if(i < wielkosc*wielkosc){
                    Skacz(i+1, nowy_x, nowy_y, q);
                    if(!q){
                        szachownica[nowy_x][nowy_y] = 0;
                        DodajUsunSasiada(nowy_x, nowy_y, 1);
                    }
                }
                else{
                    q = true;
                }                
            }while(!q);
        }

        // Wyświetlanie szachownicy
        void Pokaz(){
            for(int i = 0; i < wielkosc; ++i){
                for(int j = 0; j < wielkosc; ++j){
                    cout << szachownica[i][j] << " ";
                }
                cout << endl;
            }
            cout << endl;
        }
        ~Skoczek(){}
};

int main(){

    int wielkosc = 0, x = 0, y = 0;
    bool q = false;
    cout << "Podaj wielkosc szachownicy : ";
    cin >> wielkosc;
    
    Skoczek skoczek(wielkosc);

    x = skoczek.GetX();
    y = skoczek.GetY();
    skoczek.Skacz(2, x, y, q);
    skoczek.Pokaz();
    cout << "Koniec!" << endl;

    return 0;
}