#include <iostream>
#include <vector>

using namespace std;

class Skoczek{
    private:
        vector<int> ruch_x = {2, 1, -1, -2, -2, -1, 1, 2};
        vector<int> ruch_y = {1, 2, 2, 1, -1, -2, -2, -1};
        int pocz_x = 0, pocz_y = 0, wielkosc = 0, ruchy = 8;
        vector<vector<int>> szachownica;
    public:
        Skoczek(){}
        Skoczek(int wielkosc){
            this->wielkosc = wielkosc;
            szachownica = vector<vector<int>>(wielkosc, vector<int>(wielkosc,0));
            szachownica[pocz_x][pocz_y] = 1;
        }
        void UstawStart(int x, int y){
            szachownica[pocz_x][pocz_y] = 0;
            pocz_x = x;
            pocz_y = y;
            szachownica[pocz_x][pocz_y] = 1;
        }
        int GetX(){
            return pocz_x;
        }
        int GetY(){
            return pocz_y;
        }
        void Skacz(int i, int x, int y, bool& q){
            int u = 0, v = 0, k = 0;
            do{  
                //if(k == ruchy) break;            
                u = x + ruch_x[k];
                v = y + ruch_y[k];
                if((0 <= u && u < wielkosc) && (0 <= v && v < wielkosc)){
                    if(szachownica[u][v] == 0){
                        szachownica[u][v] = i;
                        if(i < wielkosc*wielkosc){
                            Skacz(i+1, u, v, q);
                            if(!q){
                                szachownica[u][v] = 0;
                            }
                        }
                        else{
                            q = true;
                        }
                    }                   
                }
                k++;
            }while(!q && k != ruchy);
        }
        void Pokaz(){
            for(int i = 0; i < wielkosc; ++i){
                for(int j = 0; j < wielkosc; ++j){
                    cout << szachownica[i][j] << " ";
                }
                cout << endl;
            }
            cout << endl;
        }
        ~Skoczek(){}
};

int main(){

    int wielkosc = 0, x = 0, y = 0;
    bool q = false;
    cout << "Podaj wielkosc szachownicy : ";
    cin >> wielkosc;
    
    Skoczek skoczek(wielkosc);

    x = skoczek.GetX();
    y = skoczek.GetY();
    skoczek.Skacz(2, x, y, q);
    skoczek.Pokaz();

    return 0;
}