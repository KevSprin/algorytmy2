#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

class HetmanyHeur{
    private:
        vector<int> _pole;
        vector<int> _rzad;
        vector<int> _przekatna_suma;
        vector<int> _przekatna_roznica;
        int _liczba_hetmanow = 0;

    public:
        HetmanyHeur(){}
        HetmanyHeur(int liczba_hetmanow){
            _liczba_hetmanow = liczba_hetmanow;
        }

        void GenerujUstawienie(){
            
            _pole = vector<int>(_liczba_hetmanow);
            _rzad = vector<int>(_liczba_hetmanow,-1);
            _przekatna_suma = vector<int>(2*_liczba_hetmanow-1,-1);
            _przekatna_roznica = vector<int>(2*_liczba_hetmanow-1,-1);
            for(int i = 0; i < _liczba_hetmanow; ++i){
                int rzad = rand() % _liczba_hetmanow;
                if(_rzad[rzad] >= 0){
                    i--;
                    continue;
                }
                _pole[i] = rzad;
                _rzad[rzad] += 1;
                _przekatna_suma[i + rzad] += 1;
                _przekatna_roznica[i - rzad + _liczba_hetmanow - 1] += 1;
            }
        }

        void PokazWynik(){
            vector<vector<int>> szachownica(_liczba_hetmanow, vector<int>(_liczba_hetmanow));
            
            for(int i = 0; i < _liczba_hetmanow; ++i){
                //cout << "Hetman " << i << "  na pozycji " << _pole[i] << endl; 
                szachownica[_pole[i]][i] = 1;
            }
            for(int i = 0; i < _liczba_hetmanow; ++i){
                for(int j = 0; j < _liczba_hetmanow; ++j){
                    cout << szachownica[i][j] << " ";
                }
                cout << endl;
            }
        }    

        void WykonajGradientowo(){
           
            int dokonano_zmiane = 0;
            int konflikty = WszystkieKonflikty();   
            do{
                if(konflikty == 0) break;
                dokonano_zmiane = 0;
                for(int i = 0; i < _liczba_hetmanow; ++i){
                    for(int j = i + 1; j < _liczba_hetmanow; ++j){
                        if(CzyHetmanJestAtakowany(i) || CzyHetmanJestAtakowany(j)){
                            int stara_liczba_konflikow = ObliczKonflikty(i);
                            Swap(i, j);
                            int nowa_liczba_konflikow = ObliczKonflikty(i);
                            dokonano_zmiane++;
                            // Jeśli zmiana nic nie daje to swap back
                            if(stara_liczba_konflikow <= nowa_liczba_konflikow){
                                Swap(i, j);
                                dokonano_zmiane--;
                            }
                        }
                    }
                }
            }while(dokonano_zmiane > 0);
            int wynik_konfliktow = WszystkieKonflikty();
            cout << "Nasz wynik to: " << wynik_konfliktow << " konflitky" << endl;
            if(wynik_konfliktow > 0){
                cout << "Nie znaleziono wyniku. Generuje nowa plansze i obliczam jeszcze raz." << endl;
                GenerujUstawienie();
                PokazWynik();
                WykonajGradientowo();
            }
        }

        bool CzyHetmanJestAtakowany(int i){
            return _przekatna_suma[i + _pole[i]] > 0 || _przekatna_roznica[i - _pole[i] + _liczba_hetmanow - 1] > 0 ? true : false;
        }

        void ZmianaPrzekatnych(int i, int znak){
            _przekatna_suma[i + _pole[i]] += znak;
            _przekatna_roznica[i - _pole[i] + _liczba_hetmanow - 1] += znak;
        }

        void Swap(int i, int j){

            // zmiana przekatnych po i i j
            ZmianaPrzekatnych(i, -1);
            ZmianaPrzekatnych(j, -1);

            int tmp = _pole[i];
            _pole[i] = _pole[j];
            _pole[j] = tmp;

            // zmiana przekatnych po i i j
            ZmianaPrzekatnych(i, 1);
            ZmianaPrzekatnych(j, 1);           
        }

        int ObliczKonflikty(int hetman){
            int liczba_konfliktow = 0;
            liczba_konfliktow = 2*(_przekatna_suma[hetman + _pole[hetman]] + _przekatna_roznica[hetman - _pole[hetman] + _liczba_hetmanow - 1]);
            return liczba_konfliktow;
        }

        int WszystkieKonflikty(){
            int liczba_konfliktow = 0;
            for(int i = 0; i < 2*_liczba_hetmanow-1; ++i){
                if(_przekatna_suma[i]>0) liczba_konfliktow += _przekatna_suma[i];
                if(_przekatna_roznica[i]>0) liczba_konfliktow += _przekatna_roznica[i];
            }
            return liczba_konfliktow;
        }
        ~HetmanyHeur(){
            
        }
};


int main(){
    srand(time(NULL));

    int liczba_hetmanow = 0;
    cout << "Podaj liczbe hetmanow: ";
    cin >> liczba_hetmanow;
    HetmanyHeur hetmany(liczba_hetmanow);

    hetmany.GenerujUstawienie();
    hetmany.PokazWynik();
    hetmany.WykonajGradientowo();
    hetmany.PokazWynik();

    return 0;
}