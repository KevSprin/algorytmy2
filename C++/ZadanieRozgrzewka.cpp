#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

template<typename T>
void print_vector(vector<T> values){
    if(values.empty()){
        cout << "Zbior pusty!" << endl;
        return;
    }
    for(auto &v : values){
        cout << v << " ";
    }
    cout << endl;
}

void func(vector<int> head, vector<int> tail){
    if(tail.empty()){
        print_vector(head);
        return;
    }
    for(int i = 0; i < tail.size(); ++i){
        vector<int> tmp_tail(tail), tmp_head(head);
        int value = tmp_tail[i];
        tmp_head.push_back(value);
        tmp_tail.erase(find(tmp_tail.begin(), tmp_tail.end(), value));
        func(tmp_head, tmp_tail);
    }
}

int main(){

    int input = 0;
    cout << "Podaj wartosc N: ";
    cin >> input;
    vector<int> head, tail;
    for(int i = 1; i <= input; ++i){    
        tail.push_back(i);
    }

    func(head, tail);
    system("PAUSE");
    return 0;
}