#include <iostream>
#include <vector>

using namespace std;

class NHetmany{
    private:
        vector<bool> rzad;
        vector<bool> przekatna_suma;
        vector<bool> przekatna_roznica;
        vector<int> hetmany;
        int liczba_hetmanow = 0;
        
    public:
        NHetmany(){}
        NHetmany(int liczba_hetmanow){
            this->liczba_hetmanow = liczba_hetmanow;
            hetmany = vector<int>(liczba_hetmanow);
            rzad = vector<bool>(liczba_hetmanow, true);
            przekatna_roznica = vector<bool>(2*liczba_hetmanow-1, true);
            przekatna_suma = vector<bool>(2*liczba_hetmanow-1, true);
        }
        ~NHetmany(){}
        void WykonajHetmanow(int index, bool &finisz){
            int n = liczba_hetmanow - 1;
            int j = -1;
            do{

                j = j + 1;
                finisz = false;
                if(rzad[j] && przekatna_suma[index + j] && przekatna_roznica[index - j + n]){

                    hetmany[index] = j;
                    rzad[j] = false;
                    przekatna_suma[index + j] = false;
                    przekatna_roznica[index - j + n] = false;
                    if(index < n){

                        WykonajHetmanow(index + 1, finisz);
                        if(!finisz){
                            rzad[j] = true;
                            przekatna_suma[index + j] = true;
                            przekatna_roznica[index - j + n] = true;
                        }
                                      
                    }
                    else
                        finisz = true; 
                }
            }while(!finisz && j != n);
        }   

        void PokazWynik(){
            vector<vector<int>> szachownica(liczba_hetmanow, vector<int>(liczba_hetmanow));
            
            for(int i = 0; i < liczba_hetmanow; ++i){
                cout << "Hetman " << i << "  na pozycji " << hetmany[i] << endl; 
                szachownica[i][hetmany[i]] = 1;
            }
            for(int i = 0; i < liczba_hetmanow; ++i){
                for(int j = 0; j < liczba_hetmanow; ++j){
                    cout << szachownica[i][j] << " ";
                }
                cout << endl;
            }
        }    
};

int main(){

    int liczba_hetmanow = 0;
    cout << "Podaj liczbe hetmanow N: ";
    cin >> liczba_hetmanow;

    bool finisz = false;
    NHetmany hetmany(liczba_hetmanow);
    hetmany.WykonajHetmanow(0, finisz);
    hetmany.PokazWynik();

    cout << "Skonczylem";
    return 0;
}