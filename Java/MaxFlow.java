import java.util.LinkedList;

public class MaxFlow {
    
    public Integer maxflow = 0;
    public GrafMacierz wynik;

    public void Run(Integer liczba_wierzcholkow, GrafMacierz graf, Integer zrodlo, Integer ujscie)
            throws InterruptedException {
        maxflow = 0;
        wynik = new GrafMacierz(liczba_wierzcholkow);
        Integer poprzedniki[] = new Integer[liczba_wierzcholkow];
        Integer cfp[] = new Integer[liczba_wierzcholkow];
        LinkedList<Integer> kolejka = new LinkedList<>();
        while(true){
            for(int i = 0; i < liczba_wierzcholkow; i++){
                poprzedniki[i] = -1;
            }
            poprzedniki[zrodlo] = -2;
            cfp[zrodlo] = Integer.MAX_VALUE;
            while (!kolejka.isEmpty()) {
                kolejka.pop();
            }
            kolejka.push(zrodlo);
            boolean helperflag = false;
            while(!kolejka.isEmpty()){
                Integer wierzcholek = kolejka.pop();
                for(int i = 0; i < liczba_wierzcholkow; i++){
                    Integer cost = graf.getValue(wierzcholek, i) - wynik.getValue(wierzcholek, i);
                    if(cost.equals(0) || !poprzedniki[i].equals(-1)) continue;
                    poprzedniki[i] = wierzcholek;
                    cfp[i] = Math.min(cfp[wierzcholek], cost);
                    if(i == ujscie){
                        maxflow += cfp[ujscie];
                        Integer tmp = ujscie;
                        while (!tmp.equals(zrodlo)) {
                            Integer w = poprzedniki[tmp];
                            wynik.setValue(w, tmp, wynik.getValue(w, tmp) + cfp[ujscie]);
                            wynik.setValue(tmp, w, wynik.getValue(tmp, w) - cfp[ujscie]);
                            tmp = w;
                        }
                        helperflag = true;
                    }
                    kolejka.add(i);
                }
                if(helperflag) break;
            }
            if(!helperflag) break;
        }
    }
}