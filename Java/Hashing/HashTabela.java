import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;

class HashTabela{

    private boolean const_size = false;
    private Object[] Tabela;
    private int[] TestTabela;
    private double[] TestProcenty;
    private int elementCount=0;
    private int testElementCount = 0;
    private int size = 5;
    private Object negInfinity;
    private int deletedObjects = 0;
    private int konflikty = 0;
    private int max_konflity = 0;
    DecimalFormat dc = new DecimalFormat("#.##");

    public HashTabela(){
        Tabela = new Object[size];
        negInfinity = new Object();
    }

    public HashTabela(int size){
        this.size = size;
        Tabela = new Object[size];
        TestTabela = new int[size];
        const_size = true;
        negInfinity = new Object();
    }

    // Konstruktor tabeli pomocniczej
    public HashTabela(int size, boolean helperflag){
        if(helperflag)
            this.size = size;
        Tabela = new Object[this.size];
        TestTabela = new int[this.size];
        const_size = helperflag;
        negInfinity = new Object();
    }

    // Zwieksza licznik liczb elementów w tablicy
    private void IncreaseSize(){
        elementCount++;
    }

    // Zmniejsza licznik liczb elementów w tablicy
    private void DecreaseSize(){
        elementCount--;
    }

    // Zwraca liczbę elementów w tablicy
    public int GetElementCount(){
        return elementCount;
    }

    // Zwraca wielkość tablicy
    public int GetSize(){
        return size;
    }

    // Zwraca tabele
    public Object[] GetTabela(){
        return Tabela;
    }

    // Wyswietlanie tabeli
    public void ShowTable(){
        for (Object object : Tabela) {
            if(object == null){
                System.out.println("null");
            }
            else {
                System.out.println(object.toString());
            }
        }
        System.out.println("Liczba konfliktow: " + konflikty);
        System.out.println("Najwiekszy ciąg konfliktow: " + max_konflity);
    }

    private void Clean(){
        for(int i = 0; i < size; ++i){
            if(Tabela[i].equals(negInfinity)){
                Tabela[i] = null;
            }
        }
    }

    // Powiększa tablicę jeśli będzie przepełniona
    private void Reorganizacja(){
        double tmp = (double)elementCount/(double)size;
        double upper = 0.75d, lower = 0.25d;
        
        // Opcjonalne
        double deletedObjectsBound = 0.5;
        double res = (double)(deletedObjects/size);
        if(res >= deletedObjectsBound) Clean();
        
        if(tmp < upper && tmp >= lower){
            return;
        }
        int newsize = 0;
        if(tmp >= upper){
            newsize = 2*size;
        }
        else if(tmp < lower){
            newsize = size/2;
        }
        //konflikty = 0;
        HashTabela hash = new HashTabela(newsize, true);
        for (Object object : Tabela) {
            if(object != null){
                if(object.getClass() == Object.class) {
                    deletedObjects--;
                    continue;
                }
                hash.HashInsert(object);
            }   
        }
        size = newsize;
        Tabela = hash.GetTabela();
    }

    private void ForcePowiekszenie(){
        int newsize = 2*size;
        HashTabela hash = new HashTabela(newsize, true);
        for (Object object : Tabela) {
            if(object != null){
                if(object.getClass() == Object.class) {
                    deletedObjects--;
                    continue;
                }
                hash.HashInsert(object);
            }   
        }
        size = newsize;
        Tabela = hash.GetTabela();
    }

    // Wstawia element do tablicy
    public int HashInsert(Object param){
        int i = 0;
        if(!const_size) Reorganizacja(); // sprawdzanie wypełnienia tablicy
        if(param == null) return -1;
        do{
            while(i != size){
                int j = (int)DoubleHashing(param, i);
                if(j < 0) throw new EmptyStackException();
                if(Tabela[j] == null){
                    Tabela[j] = param;
                    IncreaseSize();
                    return j;
                }
                if(Tabela[j].getClass() == Object.class){
                    deletedObjects--;
                    Tabela[j] = param;
                    IncreaseSize();
                    return j;
                }
                i++;
                konflikty++;
            }
            System.out.println("Przepełnienie tablicy!!! Reorganizacja");
            if(const_size) break; // Jeżeli jest stała wielkośc tablicy to wyjdź z pętli
            ForcePowiekszenie();
        }while(true);
        return -1;
    }

    // Szuka element w tablicy
    public int HashSearch(Object param){
        int i = 0;
        while(i != size){
            int j = (int)DoubleHashing(param, i);
            if(Tabela[j] == null){
                //System.out.println("Null " + param.toString() + "; Próby: " + i);
                return -1;
            }
            if(Tabela[j].equals(param)){
                //System.out.println("Equals " + param.toString() + ": " + i);
                return j;
            }
            i++;
        }
        System.out.println("Przeszedł całą tablicę");
        return -1;
    }

    // Usuwa element z tablicy
    public int HashDelete(Object param){
        int i = 0;
        if(!const_size) Reorganizacja(); // sprawdzanie wypełnienia tablicy
        while(i != size){
            int j = (int)DoubleHashing(param, i);
            if(Tabela[j] == null){
                return -1;
            }
            if(Tabela[j].equals(param)){
                Tabela[j] = negInfinity;
                DecreaseSize();
                deletedObjects++;
                return j;
            }
            i++;
        }
        return 0;
    }

    // Funkcja testująca funkcje haszujących 
    public void TestHashInsert(Object param){
        int i = 0;
        if(param == null) return;
        int j = (int)DoubleHashing(param, i);
        if(j < 0) throw new EmptyStackException();
        testElementCount++;
        TestTabela[j]++;
    }

    // Szuka element w tablicy (DO TESTOWANIA!)
    public int TestHashSearch(Object param){
        int i = 0;
        int counter = 1;
        while(i != size){
            int j = (int)DoubleHashing(param, i);
            if(Tabela[j] == null){
               // System.out.println("Nie znalazł "+param.toString() + "; Próbował: " + i);
                return -counter;
            }
            if(Tabela[j].equals(param)){
              // System.out.println("Znalazł "+param.toString() + "; Próbował: " + i);
                return counter;
            }
            i++;
            counter++;
        }
        //System.out.println("Przeszedł całą tablicę");
        return -1;
    }

    // Wyświetlanie testowej tabeli
    public void ShowTestTabela(){
        for(int i = 0; i < TestTabela.length; ++i){
            System.out.println("Index : " + i + " liczba wstawionych elementów : " + TestTabela[i]);
        }
    }

    // Zwraca najmniejszy procent zawartości jednego elementu
    public double GetMinProcent(){        
        return Arrays.stream(TestProcenty).min().getAsDouble();
    }

    // Zwraca największy procent zawartości jednego elementu
    public double GetMaxProcent(){
        return Arrays.stream(TestProcenty).max().getAsDouble();
    }

    // Zwraca średnią arytmetyczną
    public double GetMean(){
        double result = 0;
        for(int i = 0; i < TestProcenty.length; ++i){
            result += TestProcenty[i];
        }
        return result/TestProcenty.length;
    }

    // Zwraca różnice między min a max
    public double GetDifference(){
        return Math.abs(GetMinProcent() - GetMaxProcent());
    }

    // Zwraca wszystkie informacje na temat min, max, róznicy oraz średniej
    public String GetMinMaxMean(){
        return "Minimum = " + dc.format(GetMinProcent()) + "%\nMaximum = " + dc.format(GetMaxProcent()) + "%\nRóżnica = "+ dc.format(GetDifference()) +"%\nŚrednia = " + dc.format(GetMean()) + "%";
    }

    /*// Funkcja Haszująca int, short, long
    private long IntHash(long param, int i) {
        long wsp_long = 2654435761l;
        return Math.abs(wsp_long * param + i);
    }

    // Funkcja Haszująca liczby zmiennopozycyjne
    private long FloatHash(long param, int i) {
        return Math.abs(param + i);
    }*/

    // Haszowanie przykładowej struktury
    /*private long HashStruct(Object param, int i) {
        PrzykladowaStruktura tmp = (PrzykladowaStruktura) param;
        long intHash = IntHash(tmp.id, i);
        long floatHash = FloatHash((long) tmp.value, i);
        long doubleHash = FloatHash((long) tmp.cost, i);
        long stringHash = StringHash(tmp.name, i);
        return intHash + floatHash + doubleHash + stringHash;
    }*/

    // funkcja haszująca dla stringów
    private long StringHash(Object param, int i) {
        String tmp = (String) param;
        long hash = 0;
        for (int index = 0; index < tmp.length(); ++index) {
            hash = ((hash << 5) + hash) + (long) tmp.charAt(index);
        }
        return Math.abs(hash + i);
    }

    private long NumberHash(long a){
        a ^= (a << 13);
        a ^= (a >>> 17);
        a ^= (a << 5);
        return a;
    }

    // Funkcja sprawdzająca typy
    public long Hash(Object param, int i){
        //double wsp_double = 2654435761d;
        if(param.getClass() == Integer.class){
            int x = (int)param;
            long y = (long)x;
            long test = NumberHash(y+i);
            return Math.abs(test);
           // return IntHash(Long.valueOf((int)param), i);
        }
        else if(param.getClass() == String.class){
            return StringHash(param, i);
        }      
        else if(param.getClass() == Character.class){
            Character ch = (Character)param;
            String s = ch.toString();
            return StringHash(s, i);
        }
        else if(param.getClass() == Double.class){ 
            //long tmp1 = (long)(wsp_double*(double)param);
            double tmp = (double)param;
            long y = Double.doubleToLongBits(tmp);
            long test = NumberHash(y+i);
            return Math.abs(test);
        }
        else if(param.getClass() == Long.class){
            long y = (long)param;
            long test = NumberHash(y+i);
            return Math.abs(test);
            //return IntHash((long)param, i);
        }
        else if(param.getClass() == Float.class){
            //long tmp1 = (long)(wsp_double*(float)param);
            //return FloatHash(tmp1, i);
            float x = (float)param;
            long y = (long)Float.floatToIntBits(x);
            long test = NumberHash(y+i);
            return Math.abs(test);
        }
        else if(param.getClass() == Short.class){
            short x = (short)param;
            long y = (long)x;
            long test = NumberHash(y+i);
            return Math.abs(test);
            //return IntHash(Long.valueOf((short)param), i);
        }
        return -1;    
    }

    public long Hash1(Object param, Integer i){
        return Hash(param, i) % size;
    }

    public long Hash2(Object param, Integer i){
        return 1 + (Hash(param, i) % (size-1));
    }

    // funkcja podwójnego haszowania (z wykładu)
    public long DoubleHashing(Object param, Integer i){
        return (Hash1(param, i) + i*Hash2(param, i)) % size;
    }

    // Wypisanie informacji do pliku
    public ArrayList<String> ReturnShowInsertTestTabela(){
        ArrayList<String> result = new ArrayList<String>();
        
        TestProcenty = new double[size];
        for(int i = 0; i < TestTabela.length; ++i){
            double percent = (((double)TestTabela[i]/(double)testElementCount)*(double)100);
            TestProcenty[i] = percent;
            result.add("Index : " + i + "\tLiczba wstawionych elementów : " + TestTabela[i] + "\tZawiera " + dc.format(percent) + "%");
        }
        result.add(GetMinMaxMean());
        return result;
    }

}