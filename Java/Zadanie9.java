import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;

class Zadanie9{

    public static void main(String[] args) {
        Drzewo T = new Drzewo(0);
        T.DodajDziecko(0, 1);
        T.DodajDziecko(0, 2);
        T.DodajDziecko(0, 3);
        T.DodajDziecko(1, 4);
        T.DodajDziecko(1, 6);
        T.DodajDziecko(2, 5);
        T.DodajDziecko(2, 7);
        List<Pair<Integer,Integer>> pytania = new ArrayList<>();
        pytania.add(new Pair<>(4,6));
        pytania.add(new Pair<>(5,6));
        pytania.add(new Pair<>(1,3));
        pytania.add(new Pair<>(5,7));
        TarjanClass tarjan = new TarjanClass(T, pytania);
        tarjan.Tarjan(T.root.id);
        System.out.println(T.toString());
    }
}