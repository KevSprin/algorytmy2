import java.util.ArrayList;
import java.util.List;


class Zadanie7 {
    public static void main(String[] args) {

        // 2-CNF
        Integer zmienne = 3;
        Integer V = zmienne*2;
        GrafMacierz graf = new GrafMacierz(V);
        //Integer a[] = { 1,2,1};
        //Integer b[] = {2, -1, -2};
        //Integer a[] = { 1,-1,-1,1};
        //Integer b[] = {-2,2,-2,-3};
        Integer a[] = {1, -2, -1, 1};
        Integer b[] = {2, -3,  3, 3};
        //Integer a[] = { 1, 1, 2, 2, 3, 1, 2, 3, 4, 5, 6, -1};
        //Integer b[] = { 3, -4, -4, -5, -5, -6, -6, -6, -7, 7, 7, 7};
        graf.stworzGrafImplikacji(a, b, zmienne);
        List<List<Integer>> sss = new ArrayList<>();
        for(int i = 0; i < V; i++){
            sss.add(new ArrayList<Integer>());
        }
        SSS.SilnieSpojneSkladowe(V, graf, sss);
        for(int v = 0; v < zmienne; ++v){
            int neg_v = v + zmienne;
            for(List<Integer> skladowa : sss){
                if(skladowa.contains(v) && skladowa.contains(neg_v)){
                    System.out.println("Nie da się spełnić");
                }
            }
        }
        System.out.println("Wyrażenie jest spełnialne");
        Integer[] wartosci = SSS.ObliczWartosciLogiczne(sss, V);
        for(int i = 0; i < wartosci.length; i++){
            if(i < zmienne)
                System.out.println("Wartość dla " + (i+1) + " = " + wartosci[i]);
            else{
                Integer tmp = i-zmienne;
                System.out.println("Wartość dla !" + (tmp+1)  + " = " + wartosci[i]);
            }
                
        }
        /*graf.addEdge(0, 1);
        graf.addEdge(1, 2);
        graf.addEdge(1, 4);
        graf.addEdge(1, 5);
        graf.addEdge(2, 3);
        graf.addEdge(2, 6);
        graf.addEdge(3, 2);
        graf.addEdge(3, 7);
        graf.addEdge(4, 0);
        graf.addEdge(4, 5);
        graf.addEdge(5, 6);
        graf.addEdge(6, 5);
        graf.addEdge(6, 7);
        graf.addEdge(7, 7);
        SSS.SilnieSpojneSkladowe(V, graf);*/
        

    }
}