import java.util.List;

import javafx.util.Pair;

public class TarjanClass {

    Drzewo T;
    UnionFind U;
    Integer Przodek[];
    boolean Odwiedzony[];
    List<Pair<Integer, Integer>> Pytania;
    public TarjanClass(Drzewo T, List<Pair<Integer,Integer>> Pytania){
        this.T = T;
        this.U = new UnionFind(T.wszystkie_Nodes.size());
        this.Przodek = new Integer[T.wszystkie_Nodes.size()];
        this.Odwiedzony = new boolean[T.wszystkie_Nodes.size()];
        this.Pytania = Pytania;
    }

    private void MakeSet(Integer x){
        U.Father[x] = -1;
        U.Count[x] = 1;
    }

    public void Tarjan(Integer u){
        MakeSet(u);
        Przodek[u] = u;
        for(Drzewo.Node v : T.wszystkie_Nodes.get(u).Dzieci){
            Tarjan(v.id);
            U.Union(u, v.id);
            Przodek[U.Find(u)] = u;
        }
        Odwiedzony[u] = true;
        for(Pair<Integer, Integer> p : Pytania){
            if(CzyParaZawiera(p, u)){
                Integer v = GetV(p, u);
                if(Odwiedzony[v]){
                    System.out.println("Wspólny przodek " + u + " oraz " + v + " jest " + Przodek[U.Find(v)]);
                }
            }
        }
    }    

    private boolean CzyParaZawiera(Pair<Integer, Integer> p, Integer i){
        return p.getKey().equals(i) || p.getValue().equals(i);
    }

    private Integer GetV(Pair<Integer, Integer> p, Integer i){
        Integer V = -1;
        if(p.getKey().equals(i)){
            V = p.getValue();
        }
        else{
            V = p.getKey();
        }
        return V;
    }

}