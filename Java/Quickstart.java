import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;


class QuickStart {

    private static int[] Iteracje = {100, 200, 300, 400, 500};
    private static int TestSize = 30;
    
    public static int Max(ArrayList<Integer> l){
        int max = 0;
        if(l.isEmpty()) return -1;
        for(int i = 0; i < l.size(); ++i){
            if(max < l.get(i)) max = l.get(i);
        }
        return max;
    }

    public static int Min(ArrayList<Integer> l){
        int min = Integer.MAX_VALUE;
        if(l.isEmpty()) return -1;
        for(int i = 0; i < l.size(); ++i){
            if(min > l.get(i)) min = l.get(i);
        }
        return min;
    }

    public static double Mean(ArrayList<Integer> l){
        double mean = 0;
        if(l.isEmpty()) return -1;
        for(int i = 0; i < l.size(); ++i){
            mean += l.get(i);
        }
        return mean/(double)l.size();
    }

    // Funkcja do zapisywania wyników do pliku
    public static void ZapisDoPliku(String fileName, String dir, List<String> result){
        try{
            File file = new File(dir,fileName);
            if(!file.exists()){
                file.createNewFile();
            }
            Path path = Paths.get(dir, fileName);
            Files.write(path, result, StandardCharsets.UTF_8);
        }
        catch(IOException e){
            System.err.println("Błąd z zapisaniem pliku: " + fileName);
            System.err.println("Bład : " +  e.toString());
        }
    }

    // Insert
    // Funkcja testująca Hashowanie Stringów dla różnej liczby iteracji
    public static void TestStringHash(List<String> list){
        String dir = "WynikiStringów";
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            String fileName = "InsertString" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0 ; j < Iteracje[i]; ++j){
                h.TestHashInsert(list.get(j));
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
    }

    // Insert
    // Funkcja testująca Hashowanie Intów dla różnej liczby iteracji
    public static void TestIntHash(){
        String dir = "WynikiIntów";
        int min = -1000, max = 1000;
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            Random r = new Random();
            String fileName = "InsertInt" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0 ; j < Iteracje[i]; ++j){
                int rng = r.nextInt((max - min) + 1) + min;
                h.TestHashInsert(rng);
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
        
    }

    // Insert
    // Funkcja testująca Hashowanie Intów dla różnej liczby iteracji
    public static void TestShortHash(){
        String dir = "WynikiShortów";
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            Random r = new Random();
            String fileName = "InsertShort" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0 ; j < Iteracje[i]; ++j){
                short rng = (short)r.nextInt(Short.MAX_VALUE + 1);
                h.TestHashInsert(rng);
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
        
    }

    // Insert
    // Funkcja testująca Hashowanie Intów dla różnej liczby iteracji
    public static void TestLongHash(){
        String dir = "WynikiLongów";
        long min = -1000, max = 1000;
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            //Random r = new Random();
            String fileName = "InsertLong" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0 ; j < Iteracje[i]; ++j){
                long rng = min + (long) (Math.random() * (max - min));
                h.TestHashInsert(rng);
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
        
    }

    // Insert
    // Funkcja testująca Hashowanie Floatów dla różnej liczby iteracji
    public static void TestFloatHash(){
        String dir = "WynikiFloatów";
        float dmin = -1000, dmax = 1000;
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            Random r = new Random();
            String fileName = "InsertFloat" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0 ; j < Iteracje[i]; ++j){
                float rng = dmin + (dmax - dmin) * r.nextFloat();
                h.TestHashInsert(rng);
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
    }

    // Insert
    // Funkcja testująca Hashowanie Floatów dla różnej liczby iteracji
    public static void TestDoubleHash(){
        String dir = "WynikiDoublów";
        double dmin = -1000, dmax = 1000;
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            Random r = new Random();
            String fileName = "InsertDouble" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0 ; j < Iteracje[i]; ++j){
                double rng = dmin + (dmax - dmin) * r.nextDouble();
                h.TestHashInsert(rng);
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
    }

    public static void TestCharHash(){
        String dir = "WynikiCharów";
        int min = 33, max = 127;
        for(int i = 0; i < Iteracje.length; ++i){
            HashTabela h = new HashTabela(TestSize);
            Random r = new Random();
            String fileName = "InsertChar" + Iteracje[i] + "x" + TestSize + ".txt";
            for(int j = 0; j < Iteracje[i]; ++j){
                char c = (char)(r.nextInt((max - min) + 1) + min);
                h.TestHashInsert(c);
            }
            ZapisDoPliku(fileName, dir, h.ReturnShowInsertTestTabela());
        }
    }

    public static void TestyInt(ArrayList<Integer> train, ArrayList<Integer> test, boolean isStatic){
        int size = 500;
        HashTabela TabelaDoTestow = new HashTabela(size, isStatic);
        
        int suma_prob = 0;
        int max_konf = 0;
        ArrayList<Integer> udane = new ArrayList<Integer>();
        ArrayList<Integer> nieudane = new ArrayList<Integer>();
        for(Integer value : train){
            TabelaDoTestow.HashInsert(value);
        }

        for(Integer value : test){
            int tmp = TabelaDoTestow.TestHashSearch(value);
            if(tmp < 0){
                nieudane.add(Math.abs(tmp));
            }
            else{
                udane.add(tmp);
            }
            tmp = Math.abs(tmp);
            if(tmp > max_konf) max_konf = tmp;
            suma_prob += tmp;

        }
        PrintInfo(TabelaDoTestow, udane, nieudane, suma_prob, max_konf, isStatic);
    }

    public static void TestyString(List<String> training, List<String> test, boolean isStatic){
        int size = 500;
        HashTabela TabelaDoTestow = new HashTabela(size, isStatic);
        for (String name : training) {
            TabelaDoTestow.HashInsert(name);
        }
        
        int suma_prob = 0;
        int max_konf = 0;
        ArrayList<Integer> udane = new ArrayList<Integer>();
        ArrayList<Integer> nieudane = new ArrayList<Integer>();
        for(String name : test){
            int tmp = TabelaDoTestow.TestHashSearch(name);
            if(tmp < 0) 
                nieudane.add(Math.abs(tmp)); 
            else 
                udane.add(tmp);
            tmp = Math.abs(tmp);
            if(tmp > max_konf) max_konf = tmp;
            suma_prob += tmp;
        }

        PrintInfo(TabelaDoTestow, udane, nieudane, suma_prob, max_konf, isStatic);
    }

    public static void PrintInfo(HashTabela TabelaDoTestow, ArrayList<Integer> udane, ArrayList<Integer> nieudane, int suma_prob, int max_konf, boolean isStatic){
        double mean_udane = Mean(udane);
        double mean_nieudane = Mean(nieudane);
        double max_udane = Max(udane);
        double min_udane = Min(udane);
        double max_nieudane = Max(nieudane);
        double min_nieudane = Min(nieudane);
        
        System.out.println("Is Static Hashtable?: " + isStatic);
        System.out.println("Wielkość Tablicy: " + TabelaDoTestow.GetSize());
        System.out.println("Maksymalna liczba prób do znalezienia : " + (max_udane >= 0 ? max_udane : "nie dotyczy"));
        System.out.println("Maksymalna liczba prób do nie znalezienia : " + (max_nieudane >= 0 ? max_nieudane : "nie dotyczy"));
        System.out.println("Minimalna liczba prób do znalezienia : " + (min_udane >= 0 ? min_udane : "nie dotyczy"));
        System.out.println("Minimalna liczba prób do nie znalezienia : " + (min_nieudane >= 0 ? min_nieudane : "nie dotyczy"));
        System.out.println("Średnia wartość prób do znalezienia : " + (mean_udane >= 0 ? mean_udane : "nie dotyczy"));
        System.out.println("Średnia wartość prób do nie znalezienia : " + (mean_nieudane >= 0 ? mean_nieudane : "nie dotyczy"));      
        System.out.println("Całkowita liczba konfliktów: " + suma_prob);
        System.out.println("Maksymalna liczba konfliktów (za jednym razem): " + max_konf);
    }
    

    public static void main(String[] args) {
        System.out.println("Haszowanie!");
        
        Vector<String> tab = new Vector<String>();
        // Wczytywanie z pliku
        try{
            File myObj = new File("Words1000");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                tab.add(data);
            }
            myReader.close();
        }catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        };

        
        /*
        TestStringHash(training);
        TestIntHash();
        TestFloatHash();
        TestDoubleHash();
        TestCharHash();
        TestLongHash();
        TestShortHash();
        */
        int size = 500;
        ArrayList<Integer> train = new ArrayList<>();
        ArrayList<Integer> test = new ArrayList<>();
        Random r = new Random();
        int min = -10000, max = 10000;
        for(int i = 0 ; i < size; ++i){
            int rng1 = r.nextInt((max - min) + 1) + min;
            int rng2 = r.nextInt((max - min) + 1) + min;
            if(rng1 == rng2 || train.contains(rng1) || test.contains(rng1) || train.contains(rng2) || test.contains(rng2)) {
                i--;
                continue;
            }
            train.add(rng1);
            test.add(rng2);
        }
        System.out.println("Testowanie na zbiorze treningowym");
        TestyInt(train, train, false);
        System.out.println("=========================================================");;
        System.out.println("Testowanie na zbiorze testowym");
        TestyInt(train, test, false);
        /*int mid = tab.size()/2;
        List<String> training = tab.subList(0, mid);
        List<String> test = tab.subList(mid, tab.size());
        
        System.out.println("Testowanie na zbiorze treningowym");
        TestyString(training, training, false);
        System.out.println("=========================================================");;
        System.out.println("Testowanie na zbiorze testowym");
        TestyString(training, test, false);*/

        
    }
}

