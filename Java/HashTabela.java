import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;

class HashTabela{

    private boolean const_size = false;
    private Object[] Tabela;
    private int[] TestTabela;
    private double[] TestProcenty;
    private int elementCount=0;
    private int testElementCount = 0;
    private int size = 5;
    private Object negInfinity;
    private int konflikty = 0;
    private int max_konflity = 0;
    DecimalFormat dc = new DecimalFormat("#.##");

    public HashTabela(){
        Tabela = new Object[size];
    }

    public HashTabela(int size){
        this.size = size;
        Tabela = new Object[size];
        TestTabela = new int[size];
        const_size = true;
    }

    // Konstruktor tabeli pomocniczej
    public HashTabela(int size, boolean helperflag){
        if(helperflag)
            this.size = size;
        Tabela = new Object[this.size];
        TestTabela = new int[this.size];
        const_size = helperflag;
    }

    // Zwieksza licznik liczb elementów w tablicy
    private void IncreaseSize(){
        elementCount++;
        if(!const_size) PowiekszZmniejsz(); // sprawdzanie wypełnienia tablicy
    }

    // Zmniejsza licznik liczb elementów w tablicy
    private void DecreaseSize(){
        elementCount--;
        if(!const_size) PowiekszZmniejsz(); // sprawdzanie wypełnienia tablicy
    }

    // Zwraca liczbę elementów w tablicy
    public int GetElementCount(){
        return elementCount;
    }

    // Zwraca wielkość tablicy
    public int GetSize(){
        return size;
    }

    // Zwraca tabele
    public Object[] GetTabela(){
        return Tabela;
    }

    // Wyswietlanie tabeli
    public void ShowTable(){
        for (Object object : Tabela) {
            if(object == null){
                System.out.println("null");
            }
            else {
                System.out.println(object.toString());
            }
        }
        System.out.println("Liczba konfliktow: " + konflikty);
        System.out.println("Najwiekszy ciąg konfliktow: " + max_konflity);
    }

    // Powiększa tablicę jeśli będzie przepełniona
    private void PowiekszZmniejsz(){
        double tmp = (double)elementCount/(double)size;
        if(tmp < 0.66d && tmp >= 0.2d){
            return;
        }
        int newsize = 0;
        if(tmp >= 0.66d){
            newsize = 2*size;
        }
        else if(tmp < 0.2d){
            newsize = size/2;
        }
        konflikty = 0;
        HashTabela hash = new HashTabela(newsize, true);
        for (Object object : Tabela) {
            hash.HashInsert(object);
        }
        size = newsize;
        Tabela = hash.GetTabela();

    }

    // Wstawia element do tablicy
    public int HashInsert(Object param){
        int i = 0;
        int tmp_length = 0;
        if(param == null) return -1;
        while(i != size){
            //int j = (int)(Hash(param, i) % size);
            int j = (int)DoubleHashing(param, i);
            if(j < 0) throw new EmptyStackException();
            if(Tabela[j] == null || Tabela[j].getClass().equals(Object.class)){
                Tabela[j] = param;
                IncreaseSize();
                
                if(tmp_length > max_konflity)  max_konflity = tmp_length;
                return j;
            }
            i++;
            konflikty++;
            tmp_length++;
        }
        System.out.println("Przepełnienie tablicy!!!!!!!!!!!!!!");
        System.out.println("Koniec na : " + param.toString() + " przy liczbie konfliktow "+i);
        return 1;
    }

    // Szuka element w tablicy
    public int HashSearch(Object param){
        int i = 0;
        while(i != size){
            //int j = (int)(Hash(param, i) % size);
            int j = (int)DoubleHashing(param, i);
            if(Tabela[j] == null){
                System.out.println("Null " + param.toString() + "; Próby: " + i);
                return -1;
            }
            if(Tabela[j].equals(param)){
                System.out.println("Equals " + param.toString() + ": " + i);
                return j;
            }
            i++;
        }
        System.out.println("Przeszedł całą tablicę");
        return 0;
    }

    // Usuwa element z tablicy
    public int HashDelete(Object param){
        int i = 0;
        while(i != size){
            //int j = (int)(Hash(param, i) % size);
            int j = (int)DoubleHashing(param, i);
            if(Tabela[j] == null){
                return -1;
            }
            if(Tabela[j] == param){
                Tabela[j] = negInfinity;
                DecreaseSize();

                return j;
            }
            i++;
        }
        return 0;
    }

    // Funkcja do testowania funkcji haszujących
    public void TestHashInsert(Object param){
        int i = 0;
        if(param == null) return;
        //int j = (int)(Hash(param, i) % size);
        int j = (int)DoubleHashing(param, i);
        if(j < 0) throw new EmptyStackException();
        testElementCount++;
        TestTabela[j]++;
    }

    // Szuka element w tablicy
    public int TestHashSearch(Object param){
        int i = 0;
        int counter = 0;
        while(i != size){
            //int j = (int)(Hash(param, i) % size);
            counter++;
            int j = (int)DoubleHashing(param, i);
            if(Tabela[j] == null){
               // System.out.println("Nie znalazł "+param.toString() + "; Próbował: " + i);
                if(i == 0) return -1;
                return -i;
            }
            if(Tabela[j].equals(param)){
              // System.out.println("Znalazł "+param.toString() + "; Próbował: " + i);
                return counter;
            }
            i++;
        }
        //System.out.println("Przeszedł całą tablicę");
        return -1;
    }

    // Wyświetlanie testowej tabeli
    public void ShowTestTabela(){
        for(int i = 0; i < TestTabela.length; ++i){
            System.out.println("Index : " + i + " liczba wstawionych elementów : " + TestTabela[i]);
        }
    }

    // Zwraca najmniejszy procent zawartości jednego elementu
    public double GetMinProcent(){        
        return Arrays.stream(TestProcenty).min().getAsDouble();
    }

    // Zwraca największy procent zawartości jednego elementu
    public double GetMaxProcent(){
        return Arrays.stream(TestProcenty).max().getAsDouble();
    }

    // Zwraca średnią arytmetyczną
    public double GetMean(){
        double result = 0;
        for(int i = 0; i < TestProcenty.length; ++i){
            result += TestProcenty[i];
        }
        return result/TestProcenty.length;
    }

    // Zwraca różnice między min a max
    public double GetDifference(){
        return Math.abs(GetMinProcent() - GetMaxProcent());
    }

    // Zwraca wszystkie informacje na temat min, max, róznicy oraz średniej
    public String GetMinMaxMean(){
        return "Minimum = " + dc.format(GetMinProcent()) + "%\nMaximum = " + dc.format(GetMaxProcent()) + "%\nRóżnica = "+ dc.format(GetDifference()) +"%\nŚrednia = " + dc.format(GetMean()) + "%";
    }

    // Funkcja Haszująca int, short, long
    private long IntHash(long param, int i) {
        long wsp_long = 2654435761l;
        return Math.abs(wsp_long * param + i);
    }

    // Funkcja Haszująca liczby zmiennopozycyjne
    private long FloatHash(long param, int i) {
        return Math.abs(param + i);
    }

    // lepsza funkcja haszująca dla stringów
    private long StringHash(Object param, int i) {
        String tmp = (String) param;
        long hash = 0;
        for (int index = 0; index < tmp.length(); ++index) {
            hash = ((hash << 5) + hash) + (long) tmp.charAt(index);
        }
        return Math.abs(hash + i);
    }

    // Haszowanie przykładowej struktury
    private long HashStruct(Object param, int i) {
        PrzykladowaStruktura tmp = (PrzykladowaStruktura) param;
        long intHash = IntHash(tmp.id, i);
        long floatHash = FloatHash((long) tmp.value, i);
        long doubleHash = FloatHash((long) tmp.cost, i);
        long stringHash = StringHash(tmp.name, i);
        return intHash + floatHash + doubleHash + stringHash;
    }

    private long testHash(long a){
        a ^= (a << 13);
        a ^= (a >>> 17);
        a ^= (a << 5);
        return a;
    }

    // Funkcja sprawdzająca typy
    public long Hash(Object param, int i){
        //double wsp_double = 2654435761d;
        if(param.getClass() == Integer.class){
            return IntHash(Long.valueOf((int)param), i);
        }
        else if(param.getClass() == String.class){
            return StringHash(param, i);
        }      
        else if(param.getClass() == Character.class){
            Character ch = (Character)param;
            String s = ch.toString();
            return StringHash(s, i);
        }
        else if(param.getClass() == Double.class){ 
            //long tmp1 = (long)(wsp_double*(double)param);
            double tmp = (double)param;
            long y = Double.doubleToLongBits(tmp);
            long test = testHash(y+i);
            return Math.abs(test);
        }
        else if(param.getClass() == Long.class){
            long y = (long)param;
            long test = testHash(y+i);
            return Math.abs(test);
            //return IntHash((long)param, i);
        }
        else if(param.getClass() == Float.class){
            //long tmp1 = (long)(wsp_double*(float)param);
            //return FloatHash(tmp1, i);
            float x = (float)param;
            long y = (long)Float.floatToIntBits(x);
            long test = testHash(y+i);
            return Math.abs(test);
        }
        else if(param.getClass() == Short.class){
            short x = (short)param;
            long y = (long)x;
            long test = testHash(y+i);
            return Math.abs(test);
            //return IntHash(Long.valueOf((short)param), i);
        }
        else if(param.getClass() == PrzykladowaStruktura.class){
            return HashStruct(param, i);
        }
        return -1;    
    }

    public long Hash1(Object param, Integer i){
        return Hash(param, i) % size;
    }

    public long Hash2(Object param, Integer i){
        return 1 + (Hash(param, i) % (size-1));
    }

    public long DoubleHashing(Object param, Integer i){
        return (Hash1(param, i) + i*Hash2(param, i)) % size;
    }

    public ArrayList<String> ReturnShowInsertTestTabela(){
        ArrayList<String> result = new ArrayList<String>();
        
        TestProcenty = new double[size];
        for(int i = 0; i < TestTabela.length; ++i){
            double percent = (((double)TestTabela[i]/(double)testElementCount)*(double)100);
            TestProcenty[i] = percent;
            result.add("Index : " + i + "\tLiczba wstawionych elementów : " + TestTabela[i] + "\tZawiera " + dc.format(percent) + "%");
        }
        result.add(GetMinMaxMean());
        return result;
    }

}