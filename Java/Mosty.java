import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;

class Mosty {
    
    private static Integer DFS(Integer wierzcholek, Integer ojciec, GrafMacierz graf, Integer[] numery_wierzcholkow, Integer numer_wierzcholka, List<Pair<Integer,Integer>> mosty){
        numery_wierzcholkow[wierzcholek] = numer_wierzcholka;
        Integer low = numer_wierzcholka;
        numer_wierzcholka++;
        for(Integer sasiad : graf.getNeighbours(wierzcholek)){
            if (sasiad.equals(ojciec)) continue;
            if (!numery_wierzcholkow[sasiad].equals(0)){
                if(numery_wierzcholkow[sasiad] < low){
                    low = numery_wierzcholkow[sasiad];
                }
            }
            else{
                Integer tmp = DFS(sasiad, wierzcholek, graf, numery_wierzcholkow, numer_wierzcholka, mosty);
                if(tmp < low) {
                    low = tmp;
                }
            }
        }
        if(ojciec > -1 && low.equals(numery_wierzcholkow[wierzcholek])){
            mosty.add(new Pair<Integer,Integer>(ojciec ,wierzcholek));
        }
        return low;
    }

    public static List<Pair<Integer,Integer>> ZnajdzMosty(Integer liczba_wierzcholkow, GrafMacierz graf){
        Integer[] numery_wierzcholkow = new Integer[liczba_wierzcholkow];
        for(int i = 0; i < liczba_wierzcholkow; i++){
            numery_wierzcholkow[i] = 0;
        }
        List<Pair<Integer,Integer>> wynik = new ArrayList<>();
        for(int wierzcholek = 0; wierzcholek < liczba_wierzcholkow; wierzcholek++){
            if(numery_wierzcholkow[wierzcholek] > 0) continue;
            Integer numer_wierzcholka = 1;
            DFS(wierzcholek, -1, graf, numery_wierzcholkow, numer_wierzcholka, wynik);
        }
        return wynik;
    }
}