import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SSS{
    
    public static void DFSstack(Integer v, boolean[] visited, Stack<Integer> S, GrafMacierz graf){
        visited[v] = true;
        for(Integer u : graf.getNeighbours(v)){
            if(visited[u] == false){
                DFSstack(u, visited, S, graf);
            }
        }
        S.push(v);
    }

    public static void DFSprint(Integer v, boolean[] visited, GrafMacierz graf, List<List<Integer>> sss, Integer cn){
        visited[v] = true;
        System.out.println(v);
        sss.get(cn).add(v);
        for(Integer u : graf.getNeighbours(v)){
            if(visited[u] == false){
                DFSprint(u, visited, graf, sss, cn);
            }
        }
    }

    private static void fillFalse(boolean[] visited){
        for(int i = 0; i < visited.length; ++i){
            visited[i] = false;
        }
    }

    public static void SilnieSpojneSkladowe(Integer n, GrafMacierz graf, List<List<Integer>> sss){
        boolean[] visited = new boolean[n];
        Stack<Integer> S = new Stack<>();
        for(int v = 0; v < n; v++){
            if(visited[v] == false){
                DFSstack(v, visited, S, graf);
            }
        }
        graf.transpose();
        fillFalse(visited);
        Integer cn = 0;
        while(!S.empty()){
            Integer v = S.peek();
            S.pop();
            if(visited[v]) continue;  
            System.out.println("SSS " + cn + " : ");
            DFSprint(v, visited, graf, sss, cn);
            cn++;
        }
    }

    private static boolean checkPrzypisanie(List<Integer> skladowa, Integer[] wartosci){
        for(Integer zmienna : skladowa){
            if(!wartosci[zmienna].equals(0)){
                return true;
            }
        }
        return false;
    }

    private static Integer dowolneUstaloneV(List<Integer> skladowa, Integer[] wartosci){
        Integer result = 0;
        for(Integer zmienna : skladowa){
            if(!wartosci[zmienna].equals(0)){
                result = wartosci[zmienna];
                return result;
            }
        }
        return result;
    }

    public static Integer[] ObliczWartosciLogiczne(List<List<Integer>> sss, Integer liczba_zmiennych){
        Integer[] wartosci = new Integer[liczba_zmiennych];
        for(int i = 0; i < liczba_zmiennych; i++){
            wartosci[i] = 0;
        }
        Integer przesuniecie = liczba_zmiennych/2;
        for(List<Integer> skladowa : sss){
            if(checkPrzypisanie(skladowa, wartosci)){
                Integer dowolna = dowolneUstaloneV(skladowa, wartosci);
                for(Integer zmienna : skladowa){
                    if(wartosci[zmienna].equals(0)){
                        wartosci[zmienna] = dowolna;
                    }
                }
            }
            else{
                for(Integer zmienna : skladowa){
                    wartosci[zmienna] = -1;
                    // negacja
                    if(zmienna >= przesuniecie){
                        wartosci[zmienna-przesuniecie] = 1;
                    }
                    else{
                        wartosci[zmienna+przesuniecie] = 1;
                    }
                }
            }
        }
        return wartosci;
    }
}