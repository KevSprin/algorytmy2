class PrzykladowaStruktura{
    public int id;
    public double cost;
    public String name;
    public float value;
    public PrzykladowaStruktura(int id, double cost, String name, float value){
        this.id = id;
        this.cost = cost;
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString(){
        return "Przykladowa Struktura: [ID:"+id+"; Koszt:"+cost+"; Nazwa:"+name+"; Wartosc:"+value+"]";
    }
}