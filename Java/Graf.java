import java.util.ArrayList;

public class Graf {
    public int V, E;
    public ArrayList<Krawedz> Krawedzie;
    private int indexPointer = 0;

    public class Krawedz implements Comparable<Krawedz> {
        public Integer src, dest, cost;
        public Krawedz(int src, int dest, int cost){
            this.src = src;
            this.dest = dest;
            this.cost = cost;
        }

        @Override
        public int compareTo(Krawedz e){
            return this.cost.compareTo(e.cost);
        }
    };

    public Graf(int v, int e){
        V = v;
        E = e;
        Krawedzie = new ArrayList<Krawedz>();
    }

    public void AddKrawedz(int src, int dest, int cost){
        if(indexPointer == E){
            System.out.println("Graf jest pełny!");
            return;
        }
        Krawedz k = new Krawedz(src, dest, cost);
        Krawedzie.add(k);
        indexPointer++;
    }

}