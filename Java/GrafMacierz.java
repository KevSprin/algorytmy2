import java.util.ArrayList;
import java.util.List;

public class GrafMacierz {

    private Integer V;
    private Integer graf[][];

    public GrafMacierz(Integer V){
        this.V = V;
        graf = new Integer[V][V];
        for(int i = 0; i < V; i++){
            for(int j = 0; j < V; j++){
                graf[i][j] = 0;
            }
        }
    }

    public Integer getValue(Integer x, Integer y){
        return graf[x][y];
    }

    public void setValue(Integer x, Integer y, Integer value){
        graf[x][y] = value;
    }

    public void addEdge(Integer x, Integer y){
        graf[x][y] = 1;
    }

    public void addEdgeN(Integer x, Integer y){
        graf[x][y] = 1;
        graf[y][x] = 1;
    }

    public List<Integer> getNeighbours(Integer x){
        List<Integer> result = new ArrayList<>();
        for(int i = 0; i < V; i++){
            if(!graf[x][i].equals(0)){
                result.add(i);
            }
        }
        return result;
    }

    public boolean hasNeighgour(Integer v, Integer neighbour){
        return graf[v][neighbour].equals(1);
    }

    public void transpose(){
        Integer[][] tmp = new Integer[V][V];
        for(int i = 0; i < V; i++){
            for(int j = 0; j < V; j++){
                tmp[i][j] = graf[j][i];
            }
        }
        graf = tmp;
    }

    public Integer[][] getGraf(){
        return graf;
    }

    public void stworzGrafImplikacji(Integer[] a, Integer[] b, Integer przesuniecie){
        if(a.length != b.length){
            System.out.println("Must be same dimensions");
            return;
        }
        for(int i = 0; i < a.length; i++){
            int x = a[i];
            int y = b[i];
            int neg_x = x + przesuniecie;
            int neg_y = y + przesuniecie;
            if(y < 0){
                neg_y = -y;
                y = -y + przesuniecie;
            }
            if(x < 0){
                neg_x = -x;
                x = -x + przesuniecie;
            }
            x--; y--; neg_x--; neg_y--;
            graf[neg_x][y] = 1;
            graf[neg_y][x] = 1;
        }
    }

    @Override
    public String toString(){
        String result = "";
        for(int i = 0; i < V; i++){
            result += i + " : [";
            for(int j = 0; j < V; j++){
                result += " " +  graf[i][j] + " ";
            }
            result += "]\n";
        }
        return result;
    }
}