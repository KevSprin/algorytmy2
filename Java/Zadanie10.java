class Zadanie10{

    public static void main(String[] args) throws InterruptedException {
        Integer zrodlo = 0;
        Integer ujscie = 0;
        Integer V = 7;
        GrafMacierz graf = new GrafMacierz(V);
        ujscie = V-1;
        graf.setValue(zrodlo, 1, 9);
        graf.setValue(zrodlo, 4, 9);
        graf.setValue(1, 2, 7);
        graf.setValue(1, 3, 3);
        graf.setValue(2, 3, 4);
        graf.setValue(2, ujscie, 6);
        graf.setValue(3, ujscie, 9);
        graf.setValue(3, 5, 2);
        graf.setValue(4, 3, 3);
        graf.setValue(4, 5, 6);
        graf.setValue(5, ujscie, 8);
        /*graf.setValue(zrodlo, 1, 4);
        graf.setValue(zrodlo, 2, 3);
        graf.setValue(1, 3, 4);
        graf.setValue(3, 2, 3);
        graf.setValue(2, 4, 6);
        graf.setValue(3, ujscie, 2);
        graf.setValue(4, ujscie, 6);*/
        MaxFlow maxFlow = new MaxFlow();
        maxFlow.Run(V, graf, zrodlo, ujscie);
        System.out.println("Maksymalny przepływ wynosi: " + maxFlow.maxflow);
        System.out.println(maxFlow.wynik.toString());
    }
}