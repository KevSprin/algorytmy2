import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class GraphTest{

    public static void main(String[] args) {
        System.out.println("Testin Graph Stream");
        Graph g = new SingleGraph("Tutorial 1");
        g.addNode("S");
        g.addNode("1");
        g.addNode("2");
        g.addNode("T");

        g.addEdge("1", "S", "1");
        g.addEdge("2", "S", "2");
        g.addEdge("3", "1", "2");
        g.addEdge("4", "1", "T");
        g.addEdge("5", "2", "T");
        for(Node n : g){
            n.addAttribute("ui.label", n.getId());
        }
        g.display();
    }
}