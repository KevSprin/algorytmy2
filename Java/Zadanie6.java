import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

class Kruskal{
    public static void main(String[] args) {
        int V = 10, E = 18;
        Graf g = new Graf(V, E);
        /*g.AddKrawedz(0, 1, 13);
        g.AddKrawedz(0, 2, 8);
        g.AddKrawedz(0, 3, 1);
        g.AddKrawedz(1, 2, 15);
        g.AddKrawedz(2, 3, 5);
        g.AddKrawedz(2, 4, 3);
        g.AddKrawedz(3, 4, 4);
        g.AddKrawedz(3, 5, 5);
        g.AddKrawedz(4, 5, 2);*/
        g.AddKrawedz(0, 1, 5);
        g.AddKrawedz(0, 3, 4);
        g.AddKrawedz(0, 4, 1);
        g.AddKrawedz(1, 2, 4);
        g.AddKrawedz(1, 3, 2);
        g.AddKrawedz(2, 7, 4);
        g.AddKrawedz(2, 8, 1);
        g.AddKrawedz(2, 9, 2);
        g.AddKrawedz(3, 4, 2);
        g.AddKrawedz(3, 5, 5);
        g.AddKrawedz(3, 6, 11);
        g.AddKrawedz(3, 7, 2);
        g.AddKrawedz(4, 5, 1);
        g.AddKrawedz(5, 6, 7);
        g.AddKrawedz(6, 7, 1);
        g.AddKrawedz(6, 8, 4);
        g.AddKrawedz(7, 8, 6);
        g.AddKrawedz(8, 9, 0);
        Collections.sort(g.Krawedzie);

        UnionFind u = new UnionFind(V);
        //int zbiory = 0;
        int suma = 0;
        List<Graf.Krawedz> wynik = new ArrayList<Graf.Krawedz>();
        for(int i = 0; i < g.E; i++){
            Graf.Krawedz k = g.Krawedzie.get(i);
            int v1 = k.src;
            int v2 = k.dest;
            int subset_V1 = u.Find(v1);
            int subset_V2 = u.Find(v2);
            
            if(subset_V1 == subset_V2) continue;
            u.Union(subset_V1, subset_V2);
            wynik.add(k);
            suma += k.cost;
            if(u.IsMST())
                break;
        }

        System.out.println("Wynik MST to:");

        for(Graf.Krawedz e : wynik){
            System.out.println(e.src + " <-> " + e.dest);
        }
        
        System.out.println("Koszt to: " + suma);
        System.out.println("Done!");
    }
}