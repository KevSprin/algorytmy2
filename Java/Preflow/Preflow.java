import java.util.LinkedList;

public class Preflow {
    
    public GrafMacierz G;
    public GrafMacierz Gf; // residual
    public GrafMacierz F; // flow
    private LinkedList<Integer> excess_vertices; // kolejka
    private Integer[] height; // wysokości wierzchołków
    private Integer[] excess; // aktywności wierzchołków
    private Integer V = 0;
    private Integer source;
    private Integer target;
    private boolean useBFS = false;

    public Preflow(GrafMacierz G, boolean useBFS){
        this.G = G;
        V = G.V;
        source = 0;
        target = V - 1;
        excess_vertices = new LinkedList<>();
        height = new Integer[V];
        excess = new Integer[V];
        F = new GrafMacierz(V);
        Gf = new GrafMacierz(V);
        for(int i = 0; i < V; i++){
            for(int j = 0; j < V; j++){
                Gf.Macierz[i][j] = G.Macierz[i][j];
            }
        }
        this.useBFS = useBFS;
    }

    private Drzewo drzewo;
    private int dist = 0;
    private LinkedList<Integer> bfsQueue;
    private boolean Expand(LinkedList<Integer> fringe, Integer node, Integer goal){
        for(Integer child : G.GetSasiadow(node)){
            if(!fringe.contains(child)){
                fringe.add(child);
                drzewo.DodajDziecko(node, child);
            }
        }
        for(Drzewo.Node child : drzewo.GetDzieci(node)){
            if(child.id.equals(goal)){
                dist = child.depth;
                return true;
            }
        }
        return false;
    }

    private boolean DoBFS(LinkedList<Integer> fringe, Integer node, Integer goal){
        if(node == null){
            return false;
        }
        if(node == goal){
            return true;
        }
        if(Expand(fringe, node, goal)){
            return true;
        }
        //fringe.add(null);
        while(!fringe.isEmpty()){
            Integer child = fringe.remove();
            if(DoBFS(fringe, child, goal)){
                return true;
            }
        }
        return false;
    }

    private void InitializeHeightBFS(){
        for(int v = 1; v != target; v++){
            bfsQueue = new LinkedList<Integer>();
            drzewo = new Drzewo(v);
            dist = 1;
            DoBFS(bfsQueue, v, target);
            height[v] = dist;
        }
    }

    // without BFS
    private void Initialize_Preflow(){
        for(int i = 0; i < V; i++){
            height[i] = 0;
            excess[i] = 0;
        }
        height[source] = V;
        for(Integer u : G.GetSasiadow(source)){
            int cost = G.Macierz[source][u];
            F.Macierz[source][u] = cost;
            F.Macierz[u][source] = -cost;
            excess[u] = cost;
            excess[source] -= cost;
            Gf.Macierz[source][u] -= cost;
            Gf.Macierz[u][source] += cost;
            if(u != target) excess_vertices.add(u);
        }
        if(useBFS){
            InitializeHeightBFS();
        }
    }

    public Integer pushCounter = 0;
    private void Push(int u, int v){
        pushCounter++;
        int gf_cost = Gf.Macierz[u][v];
        int d = Math.min(excess[u], gf_cost);
        F.Macierz[u][v] += d;
        F.Macierz[v][u] -= d;
        excess[u] -= d;
        excess[v] += d;
        Gf.Macierz[u][v] -= d;
        Gf.Macierz[v][u] += d;   
    }

    public Integer relabelCounter = 0;
    private void Relabel(Integer u){
        relabelCounter++;
        int d = Integer.MAX_VALUE;
        for(Integer v : Gf.GetSasiadow(u)){
            if(height[u] <= height[v]){
                d = Math.min(d, height[v]);
            }
        }
        if (d < Integer.MAX_VALUE)
            height[u] = d + 1;   
    }

    private Integer dopuszczalny_sasiad = -1;

    private boolean IsDopuszczalnyVertex(Integer u){
        for(Integer v : Gf.GetSasiadow(u)){
            if(height[u] == height[v] + 1){
                dopuszczalny_sasiad = v;
                return true;
            }
        }
        dopuszczalny_sasiad = -1;
        return false;
    }

    public Integer Run(){
        Initialize_Preflow();
        while(!excess_vertices.isEmpty()){
            Integer u = excess_vertices.pop(); 
            while(excess[u] > 0 && IsDopuszczalnyVertex(u)){ // czy wierzchołek jest nadmiarowy
                Push(u, dopuszczalny_sasiad);
                if(dopuszczalny_sasiad != source && dopuszczalny_sasiad != target && !excess_vertices.contains(dopuszczalny_sasiad)) excess_vertices.add(dopuszczalny_sasiad);
            }
            if(excess[u] > 0){
                Relabel(u);
                excess_vertices.add(u);
            }
        }
        return excess[target];
    }

    public String GetHeight(){
        String result = "Wysokość wierzchołków:\n";
        for(int i = 0; i < height.length; i++){
            result += i + " : " + height[i] + "\n";
        }
        return result;
    }

    public String GetExcess(){
        String result = "Nadmiary wierzchołków:\n";
        for(int i = 0; i < excess.length; i++){
            result += i + " : " + excess[i] + "\n";
        }
        return result;
    }
}