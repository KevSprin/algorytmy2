import java.util.ArrayList;
import java.util.List;

public class GrafMacierz {

    public int V;
    public int[][] Macierz;

    public GrafMacierz(int V){
        this.V = V;
        Macierz = new int[V][V];
    }

    public void Transponowanie(){
        int[][] tmp = new int[V][V];
        for(int i = 0; i < V; i++){
            for(int j = 0; j < V; j++){
                tmp[i][j] = Macierz[j][i];
            }
        }
        Macierz = tmp;
    }
    
    public List<Integer> GetSasiadow(Integer u){
        List<Integer> result = new ArrayList<>();
        for(int v = 0; v < V; v++){
            if(Macierz[u][v] != 0){
                result.add(v);
            }
        }
        return result;
    }

    @Override
    public String toString(){
        String result = "";
        for(int i = 0; i < V; i++){
            result += (i+1) + " : ";
            for(int j = 0; j < V; j++){
                result += Macierz[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }
}