import java.util.ArrayList;
import java.util.List;

public class Drzewo {
    class Node{
        Integer id;
        Node ojciec;
        List<Node> Dzieci;
        boolean IsVisited = false;
        Integer depth = 0;
        public Node(Integer id){
            this.id = id;
            this.ojciec = null;
            Dzieci = new ArrayList<>();
        }

        public Node(Integer id, Integer depth){
            this.id = id;
            this.ojciec = null;
            Dzieci = new ArrayList<>();
            this.depth = depth;
        }

        public Node(Integer id, Node ojciec){
            this.ojciec = ojciec;
            this.id = id;
            this.depth = ojciec.GetDepth() +1;
            Dzieci = new ArrayList<>();
        }

        public void DodajDziecko(Node id){
            Dzieci.add(id);
        }

        public List<Node> GetDzieci(){
            return Dzieci;
        }

        public Integer GetDepth(){
            return depth;
        }

        @Override
        public String toString(){
            return id.toString();
        }
    }
    public Node root;
    public List<Node> wszystkie_Nodes;

    public Drzewo(Integer id){
        root = new Node(id, 0);
        wszystkie_Nodes = new ArrayList<>();
        wszystkie_Nodes.add(root);
    }

    private int GetIndexOfNode(Integer id){
        for(int i = 0; i < wszystkie_Nodes.size(); i++){
            if(wszystkie_Nodes.get(i).id.equals(id)) return i;
        }
        return -1;
    }

    public List<Node> GetDzieci(Integer node){
        int index = GetIndexOfNode(node);
        Node ojciec = wszystkie_Nodes.get(index);
        return ojciec.GetDzieci();
    }

    public void DodajDziecko(Integer node, Integer id){
        int index = GetIndexOfNode(node);
        Node ojciec = wszystkie_Nodes.get(index);
        Node n = new Node(id, ojciec);
        wszystkie_Nodes.get(index).DodajDziecko(n);
        wszystkie_Nodes.add(n);
    }

    @Override
    public String toString(){
        String result = "";
        for(Node n : wszystkie_Nodes){
            result += "Id : " + n.id + "\tDzieci: " + n.Dzieci.toString() + "\n";
        }
        return result;
    } 
}