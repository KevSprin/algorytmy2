import java.util.ArrayList;
import java.util.List;

public class Graf {
    public int V, E;
    public ArrayList<Krawedz> Krawedzie;
    public int indexPointer = 0;

    public class Krawedz implements Comparable<Krawedz> {
        public int src, dest, cost;
        public Krawedz(int src, int dest, int cost){
            this.src = src;
            this.dest = dest;
            this.cost = cost;
        }

        @Override
        public int compareTo(Krawedz e){
            Integer tmp = e.cost;
            return tmp.compareTo(cost);
        }

        public boolean Contains(int vertex){
            return vertex == src;
        }

        public boolean Contains(int v1, int v2){
            return (v1 == src) && (v2 == dest);
        }

        public Integer GetSasiad(int vertex){
            if(src == vertex){
                return dest;
            }
            return -1;
        }

        public void Swap(){
            int tmp = src;
            src = dest;
            dest = tmp;
        }
    };

    public Graf(int v, int e){
        V = v;
        E = e;
        Krawedzie = new ArrayList<Krawedz>();
    }

    public Graf(Graf G){
        this.V = G.V;
        this.E = G.E; 
        Krawedzie = new ArrayList<Krawedz>();
        for(int i = 0; i < G.Krawedzie.size(); i++){
            Krawedz o = G.Krawedzie.get(i);
            Krawedz k = new Krawedz(o.src, o.dest, o.cost);
            Krawedzie.add(k);
        }
        this.indexPointer = G.indexPointer;
    }

    public List<Integer> GetSasiadow(Integer v){
        List<Integer> result = new ArrayList<>();
        for(Krawedz e : Krawedzie){
            int sasiad = e.GetSasiad(v);
            if(sasiad != -1){
                result.add(sasiad);
            }
        }
        return result;
    }

    public Integer GetKoszt(Integer u, Integer v){
        for(Krawedz k : Krawedzie){
            if(k.Contains(u,v)){
                return k.cost;
            }
        }
        return 0;
    }

    public void SetKoszt(Integer u, Integer v, Integer cost){
        for(Krawedz k : Krawedzie){
            if(k.Contains(u, v)){
                k.cost = cost;
                return;
            }
        }
    }

    public void AddKoszt(Integer u, Integer v, Integer cost){
        for(Krawedz k : Krawedzie){
            if(k.Contains(u, v)){
                k.cost += cost;
                return;
            }
        }
    }

    public void AddKrawedz(int src, int dest, int cost){
        if(indexPointer == E){
            System.out.println("Graf jest pełny!");
            return;
        }
        Krawedz k = new Krawedz(src, dest, cost);
        Krawedzie.add(k);
        indexPointer++;
    }

    public void Transponuj(){
        for(Krawedz k : Krawedzie){
            k.Swap();
        }
    }

}