import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ZadaniePreflow {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Algorytm Preflow!");
        boolean useBFS = true;
        Scanner sc = new Scanner(new BufferedReader(new FileReader("Ahuja.txt")));
        boolean first = true;
        int v = 0;
        int i = 0;
        GrafMacierz g = null;
        while(sc.hasNextLine()){
            if(first){
                String s = sc.nextLine();
                v = Integer.parseInt(s);
                first = false;
                g = new GrafMacierz(v);
                continue;
            }
            String[] line = sc.nextLine().split(" ");
            for(int j = 0; j < line.length; j++){
                g.Macierz[i][j] = Integer.parseInt(line[j]);
            }
            i++;
        }


        /*Integer V = 6;
        
        GrafMacierz G = new GrafMacierz(V);
        /*G.Macierz[0][1] = 5;
        G.Macierz[0][2] = 4;
        G.Macierz[1][2] = 3;
        G.Macierz[1][3] = 1;
        G.Macierz[2][3] = 5;
        G.Macierz[0][1] = 15;
        G.Macierz[0][3] = 4;
        G.Macierz[1][2] = 12;
        G.Macierz[2][3] = 3;
        G.Macierz[3][4] = 10;
        G.Macierz[4][1] = 5;
        G.Macierz[2][5] = 7;
        G.Macierz[4][5] = 10;*/
        Preflow preflow = new Preflow(g, useBFS);
        int result = preflow.Run();
        System.out.println("Wynik maksymalnego przepływu wynosi : " + result);
        System.out.println("Graf rezydualny: ");
        System.out.println(preflow.Gf.toString());
        System.out.println("Graf przepływu: ");
        System.out.println(preflow.F.toString());
        System.out.println(preflow.GetHeight());
        System.out.println(preflow.GetExcess());
        System.out.println("Using BFS : " + useBFS);
        System.out.println("Push counter: " + preflow.pushCounter);
        System.out.println("Relabel counter: " + preflow.relabelCounter);
    }
}