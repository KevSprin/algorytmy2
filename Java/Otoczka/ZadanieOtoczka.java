import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.Collections;
import java.util.Comparator;

public class ZadanieOtoczka {
    public static double eps = 1e-9;

    public static double det(Point a, Point b, Point c){
        double x = (b.getX() - a.getX()) * (c.getY() - a.getY()) - (b.getY() - a.getY()) * (c.getX() - a.getX());
        return x; 
    }

    public static int sign(double x){
        int s = x < -eps ? -1 : (x > eps ? 1 : 0);
        return s;
    }

    public static int strona(Point a, Point b, Point c){
        return sign(det(a,b,c));
    }

    public static long cross(Point a, Point b, Point c){
        long x =  (b.getX() - a.getX()) * (long)(c.getY() - a.getY()) - (b.getY() - a.getY()) * (long)(c.getX() - a.getX());
        return x;
    }

    

    // główny algorytm otoczki
    public static List<Point> runOtoczka(List<Point> Points){
        int n = Points.size(), k = 0;
        int right = 1;
        Point[] hull = new Point[2*n];

        //Comparator<Point> comp = Comparator.comparing(point -> point.getX());
        //comp = comp.thenComparing(Comparator.comparing(point -> point.getY()));

        //Points = Points.stream().sorted(comp).collect(Collectors.toList());
        //Collections.sort(Points);
        Collections.sort(Points, new PointComparator());

        // górna otoczka
        for(int i = 0; i < n; ++i){
            while(k >= 2 && strona(hull[k-2], hull[k-1], Points.get(i)) != right){
                k--; 
            }
            hull[k++] = Points.get(i);
        }
        // dolna otoczka
        for(int i = n - 2, t = k + 1; i >= 0; i--){
            while(k >= t && strona(hull[k-2], hull[k-1], Points.get(i)) != right){
                k--;
            }
            hull[k++] = Points.get(i);
        }
        // Filtrowanie nieistotnych puntków
        List<Point> result = new ArrayList<>();
        for(int i = 0; i < k; i++){
            result.add(hull[i]);
        }
        return result;
    }
    
    // Funkcja generująca losowe punkty na płaszczyźnie
    public static void GeneratePoints(int size, int xrange, int yrange, List<Point> Points){
        System.out.println("Generating points..");
        Random r = new Random();
        int min = 1;
        for(int i = 0; i < size; ++i){
            int x = r.nextInt((xrange - min) + 1) + min;
            int y = r.nextInt((yrange - min) + 1) + min;
            Point p = new Point(x , y);
            Points.add(p);
        }
        System.out.println("Generating done!");
    }
    
    public static void main(String[] args) {
        int size = 100000000;
        int width = 600;
        int height = 600;
        List<Point> points = new ArrayList<>();

        // Generowanie losowych puntków
        GeneratePoints(size, width, height, points);   
        
        // Do obliczenia czasu
        System.out.println("Calculating the convex hull..");
        long start = System.currentTimeMillis();
        List<Point> resultPoints = runOtoczka(points);
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println("Finished convex hull calculation in " + sec + " seconds.");

        // Wyświetlanie
        //Window w = new Window(width, height, points, resultPoints);
        //w.setVisible(true);
        System.out.println("End!");
    }
}

class PointComparator implements Comparator<Point>{
    public int compare(Point p1, Point p2){
        int pointComp = p1.getX().compareTo(p2.getX());
        return (pointComp == 0) ? p1.getY().compareTo(p2.getY()) : pointComp;
    }
}
