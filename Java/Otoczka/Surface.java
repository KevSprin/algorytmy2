import javax.swing.JPanel;
import java.awt.*;
import java.awt.geom.*;
import java.util.List;

public class Surface extends JPanel {

    private List<Point> Points;
    private List<Point> resultPoints;

    public Surface(List<Point> Points){
        this.Points = Points;
    }

    public Surface(List<Point> Points, List<Point> resultPoints){
        this.Points = Points;
        this.resultPoints = resultPoints;
    }

    private void doDrawing(Graphics g) {

        int size = 7;
        int offset = size/2; 
        Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(Color.BLACK);
        for(Point p : Points){
            int x = p.getX();
            int y = p.getY();
            Ellipse2D circle = new Ellipse2D.Double();
            circle.setFrame(x, y, size, size);
            g2d.fill(circle);
        }
        if(resultPoints != null){
            g2d.setPaint(Color.RED);
            g2d.setStroke(new BasicStroke(4));
            for(int i = 0; i < resultPoints.size() - 1; ++i){
                int px1 = resultPoints.get(i).getX();
                int py1 = resultPoints.get(i).getY();
                int px2 = resultPoints.get(i + 1).getX();
                int py2 = resultPoints.get(i + 1).getY();
                g2d.drawLine(px1 + offset, py1 + offset, px2 + offset, py2 + offset);
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);
        doDrawing(g);
    }
}