public class Point implements Comparable<Point>{

    private Integer x;
    private Integer y;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public Integer getX(){
        return x;
    }

    public Integer getY(){
        return y;
    }

    @Override
    public String toString(){
        return "(" + x + ", " + y + ")";
    }

    // Dla sortowania względem x
    @Override
    public int compareTo(Point p){
        return x.compareTo(p.getX());
    }
}