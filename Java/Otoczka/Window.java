import javax.swing.JFrame;
import java.util.List;

public class Window extends JFrame{
    
    private Integer width;
    private Integer height;
    private List<Point> Points;
    private boolean drawLines = false;
    private List<Point> resultPoints;
    private final int offset = 100;

    public Window(int width, int height, List<Point> Points){
        this.width = width + offset;
        this.height = height + offset;
        this.Points = Points;
        initUI();
    }

    public Window(int width, int height, List<Point> Points, List<Point> resultPoints){
        this.width = width + offset;
        this.height = height + offset;
        this.Points = Points;
        this.resultPoints = resultPoints;
        drawLines = true;
        initUI();
    }

    private void initUI(){
        Surface surface;
        if(drawLines)
            surface = new Surface(Points, resultPoints);
        else
            surface = new Surface(Points);
        add(surface);

        setTitle("Convex-Hull");
        setSize(width, height);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}