import java.util.ArrayList;
import java.util.List;

import javafx.util.Pair;

class Zadanie8 {

    public static void main(String[] args) {
        Integer V = 15;
        GrafMacierz graf = new GrafMacierz(V);
        List<List<Integer>> sss = new ArrayList<>();
        for(int i = 0; i < V; i++){
            sss.add(new ArrayList<Integer>());
        }
        
        /*graf.addEdgeN(0, 1);
        graf.addEdgeN(0, 2);
        graf.addEdgeN(0, 3);
        graf.addEdgeN(1, 2);
        graf.addEdgeN(3, 4);
        graf.addEdgeN(3, 5);
        graf.addEdgeN(4, 5);*/

        graf.addEdgeN(0, 1);
        graf.addEdgeN(1, 3);
        graf.addEdgeN(1, 4);
        graf.addEdgeN(2, 4);
        graf.addEdgeN(2, 5);
        graf.addEdgeN(3, 4);
        graf.addEdgeN(3, 6);
        graf.addEdgeN(4, 8);
        graf.addEdgeN(5, 8);
        graf.addEdgeN(6, 7);
        graf.addEdgeN(7, 9);
        graf.addEdgeN(7, 10);
        graf.addEdgeN(7, 11);
        graf.addEdgeN(10, 11);
        graf.addEdgeN(10, 12);
        graf.addEdgeN(11, 14);
        graf.addEdgeN(12, 13);
        graf.addEdgeN(13, 14);

        List<Integer> punktyArtykulacji = PunktyArtykulacji.ZnajdzPunktyArtykulacji(V, graf);
        List<Pair<Integer,Integer>> mosty = Mosty.ZnajdzMosty(V, graf);
        System.out.println("Punktami artykulacji są:");
        for(Integer p : punktyArtykulacji){
            System.out.print(p.toString() + " ");
        }
        System.out.println("");
        for(Pair<Integer,Integer> m : mosty){
            System.out.println("Most : (" + m.getKey() + ", " + m.getValue() + ")");
        }
        
        

        /*graf.addEdge(0, 1);
        graf.addEdge(1, 2);
        graf.addEdge(1, 4);
        graf.addEdge(1, 5);
        graf.addEdge(2, 3);
        graf.addEdge(2, 6);
        graf.addEdge(3, 2);
        graf.addEdge(3, 7);
        graf.addEdge(4, 0);
        graf.addEdge(4, 5);
        graf.addEdge(5, 6);
        graf.addEdge(6, 5);
        graf.addEdge(6, 7);
        graf.addEdge(7, 7);
        SSS.SilnieSpojneSkladowe(V, graf);*/
    }

}