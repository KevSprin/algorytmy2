import java.util.ArrayList;
import java.util.List;

public class PunktyArtykulacji {

    private static Integer DFS(Integer wierzcholek, Integer ojciec, GrafMacierz graf, Integer[] numery_wierzcholkow, Integer numer_wierzcholka, List<Integer> punkty_artykulacji){
        numery_wierzcholkow[wierzcholek] = numer_wierzcholka;
        Integer low = numer_wierzcholka;
        numer_wierzcholka++;
        boolean test = false;
        for(Integer sasiad : graf.getNeighbours(wierzcholek)){
            if(sasiad.equals(ojciec)) continue;
            if (!numery_wierzcholkow[sasiad].equals(0)){
                if(numery_wierzcholkow[sasiad] < low){
                    low = numery_wierzcholkow[sasiad];
                }
            }
            else{
                Integer tmp = DFS(sasiad, wierzcholek, graf, numery_wierzcholkow, numer_wierzcholka, punkty_artykulacji);
                if(tmp < low){
                    low = tmp;
                }
                if(tmp.equals(numery_wierzcholkow[wierzcholek])){
                    test = true;
                }
            }
        }
        if (test) {
            punkty_artykulacji.add(wierzcholek);
        }
        return low;
    }

    public static List<Integer> ZnajdzPunktyArtykulacji(Integer liczba_wierzcholkow, GrafMacierz graf){
        Integer[] numery_wierzcholkow = new Integer[liczba_wierzcholkow];
        for(int i = 0; i < liczba_wierzcholkow; i++){
            numery_wierzcholkow[i] = 0;
        }
        List<Integer> wynik = new ArrayList<>();
        for(int wierzcholek = 0; wierzcholek < liczba_wierzcholkow; wierzcholek++){
            if(numery_wierzcholkow[wierzcholek] > 0) continue;
            Integer numer_wierzcholka = 1; 
            numery_wierzcholkow[wierzcholek] = numer_wierzcholka;
            numer_wierzcholka++;
            Integer liczba_synow = 0;        
            for(Integer sasiad : graf.getNeighbours(wierzcholek)){
                if(numery_wierzcholkow[sasiad] > 0) continue;
                liczba_synow++;
                DFS(sasiad, wierzcholek, graf, numery_wierzcholkow, numer_wierzcholka, wynik);
            }
            if(liczba_synow > 1){
                wynik.add(wierzcholek);
            }
        }
        return wynik;
    }
    
}