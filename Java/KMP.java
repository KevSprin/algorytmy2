public class KMP{
    
    /*private static Integer[] GetKMPNext(String wzorzec){
        Integer m = wzorzec.length();
        Integer result[] = new Integer[m+1];
        Integer i = 0, j = -1;
        result[i] = j;
        while(i < m){
            try {
                while(j > -1 && wzorzec.charAt(i) != wzorzec.charAt(j)){
                    j = result[j];
                }
            } catch (StringIndexOutOfBoundsException e) {
                
            }
            i++;
            j++;
            boolean helperflag = false;
            try{
                if(wzorzec.charAt(i) == wzorzec.charAt(j)){
                    result[i] = result[j];
                    helperflag = true;
                }
            }
            catch(Exception e){

            }
            
            if(!helperflag){
                result[i] = j;
            }
        }
        return result;
    } */

    private static Integer[] GetKmpNext(String wzorzec){
        Integer M = wzorzec.length();
        Integer KmpNext[] = new Integer[M+1];
        Integer b = -1;
        KmpNext[0] = -1;
        for(int i = 1; i <= M; i++){
            try{
                while(b > -1 && wzorzec.charAt(b) != wzorzec.charAt(i - 1)) b = KmpNext[b];
            }catch(Exception e){

            }
            b++;
            if(i == M || wzorzec.charAt(i) != wzorzec.charAt(b)) KmpNext[i] = b;
            else KmpNext[i] = KmpNext[b];
        }
        return KmpNext;
    }

    public static void runKMP(String lancuch, String wzorzec){
        Integer KmpNext[] = GetKmpNext(wzorzec);
        Integer N = lancuch.length();
        Integer M = wzorzec.length();
        Integer b = 0;
        System.out.print("Znalezione w [ ");
        for(int i = 0; i < N; i++){
            while(b > -1 && wzorzec.charAt(b) != lancuch.charAt(i)) 
                b = KmpNext[b];
            if(++b == M){
                System.out.print(i - b + 1 + " ");
                b = KmpNext[b];
            }
        }
        System.out.print("]");
    }

    /*public static void runKMP(String lancuch, String wzorzec){
        Integer KmpNext[] = GetKMPNext(wzorzec);
        Integer m = KmpNext.length;
        Integer n = lancuch.length();
        Integer i = 0, j = 0;
        while(j < n){
            try {
                while(i > -1 && wzorzec.charAt(i) != lancuch.charAt(j)){
                    i = KmpNext[i];
                }
            } catch (StringIndexOutOfBoundsException e) {
                
            }
            i++;
            j++;
            if(i >= m){
                System.out.println(j - i);
                i--;
                i = KmpNext[i];
            }
        }
    }*/
}