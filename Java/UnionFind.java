import java.util.ArrayList;

public class UnionFind {

    public Integer Count[];
    public Integer Name[];
    public Integer Father[];
    public Integer Root[];
    public Integer VertexCount;
    public Integer EdgeCount = 0;
    public ArrayList<Integer> LIST;

    public UnionFind(int V){
        VertexCount = V;
        Count = new Integer[VertexCount];
        Name = new Integer[VertexCount];
        Father = new Integer[VertexCount];
        Root = new Integer[VertexCount];
        for(int i = 0; i < V; i++){
            Count[i] = 1;
            Name[i] = i;
            Father[i] = -1;
            Root[i] = i;
        }
        LIST = new ArrayList<Integer>();
    }

    public boolean IsMST(){
        return EdgeCount.equals(VertexCount-1);
    }

    public boolean Union(int i, int j){
        // w union będzie sprawdzanie cykli
        if(Count[Root[i]] > Count[Root[j]]){
            int tmp = Root[j];
            Root[j] = Root[i];
            Root[i] = tmp;
        }
        int large = Root[j];
        int small = Root[i];
        Father[small] = large;
        Count[large] = Count[large] + Count[small];
        Name[large] = j;
        Root[j] = large;
        EdgeCount++;
        return true;
    }

    public int Find(int i){
        LIST.clear();
        int v = i;
        while(Father[v] != -1){
            LIST.add(v);
            v = Father[v];
        }
        for(int w = 0; w < LIST.size(); w++){
            int index = LIST.get(w);
            Father[index] = v;
        }
        return Name[v];
    }
}